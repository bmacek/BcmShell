#include "BcmShell/lumi/LumiBcidMap.h"

#include "CatShell/core/CatService.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiBcidMap, CAT_NAME_BCMLUMIBCIDMAP, CAT_TYPE_BCMLUMIBCIDMAP);

LumiBcidMap::LumiBcidMap()
{
}

LumiBcidMap::~LumiBcidMap()
{
}

void LumiBcidMap::cat_stream_out(std::ostream& output)
{
	LumiPeriod::cat_stream_out(output);

	output.write((char*)&_map_id, sizeof(std::uint8_t));
	CatService::write7bitEncoded(output, _orbits);
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
		CatService::write7bitEncoded(output, _counters[i]);
}

void LumiBcidMap::cat_stream_in(std::istream& input)
{
	LumiPeriod::cat_stream_in(input);
	input.read((char*)&_map_id, sizeof(std::uint8_t));
	_orbits = CatService::read7bitEncoded(input);
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
		_counters[i] = CatService::read7bitEncoded(input);
}

void LumiBcidMap::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	LumiPeriod::cat_print(output, padding); output << "Accumulated BCM BCID map." << endl;
	output << padding << "Map Id   : " << (int)_map_id << endl;
	output << padding << "Orbits   : " << _orbits << endl;
	output << padding << "Counters : ";

	std::uint16_t i=0;
	for(std::uint16_t r=0; i<ORBIT_LENGTH; ++r)
	{
		sprintf(text, "%5u: ", i);
		output << endl << padding << text;
		for(; i<10*r+10 && i<ORBIT_LENGTH;i++)
		{
			if(_counters[i])
			{
				sprintf(text, "%10u,", _counters[i]);
				output << text;
			}
			else
				output << "        . ," ;
		}
	}
}

void LumiBcidMap::read_bcm_native(std::istream& input)
{
	LumiPeriod::read_bcm_native(input);
	input.read((char*)&_map_id, sizeof(std::uint8_t));
	_orbits = CatService::read7bitEncoded(input);
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
		_counters[i] = CatService::read7bitEncoded(input);
}

StreamSize LumiBcidMap::cat_stream_get_size() const
{ 
	StreamSize size = LumiPeriod::cat_stream_get_size();
	size += sizeof(std::uint8_t);
	size += CatService::lengtOf7bitEncoded(_orbits);
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
		size += CatService::lengtOf7bitEncoded(_counters[i]);

	return size;
}

void LumiBcidMap::reset(std::uint8_t map_id)
{
        _map_id = map_id;
        _orbits = 0;
        for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
                _counters[i] = 0;
}

void LumiBcidMap::add(CatPointer<LumiBcidMap>& map)
{
	_map_id = map->_map_id;
	_orbits += map->_orbits;
	for(std::uint16_t i=0; i<ORBIT_LENGTH; ++i)
		_counters[i] += map->_counters[i];
}

} // namespace BcmShell
