#include "BcmShell/lumi/LumiReadingRate.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiReadingRate, CAT_NAME_BCMLUMIREADINGRATE, CAT_TYPE_BCMLUMIREADINGRATE);

LumiReadingRate::LumiReadingRate()
{
}

LumiReadingRate::~LumiReadingRate()
{
}

void LumiReadingRate::cat_stream_out(std::ostream& output)
{
	LumiPeriod::cat_stream_out(output);
	CatService::write7bitEncoded(output, _counts);
	CatService::write7bitEncoded(output, _orbits);
}

void LumiReadingRate::cat_stream_in(std::istream& input)
{
	LumiPeriod::cat_stream_in(input);
	_counts = CatService::read7bitEncoded(input);
	_orbits = CatService::read7bitEncoded(input);
}

void LumiReadingRate::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	LumiPeriod::cat_print(output, padding); output << "Rate period." << endl;
	output << padding << "Hits:   " << _counts << endl;
	output << padding << "Orbits: " << _orbits << endl;
	output << padding << "Rate:   " << ((double)_counts)/_orbits << endl;
}

void LumiReadingRate::read_bcm_native(std::istream& input)
{
	LumiPeriod::read_bcm_native(input);
	_counts = CatService::read7bitEncoded(input);
	_orbits = CatService::read7bitEncoded(input);
}

} // namespace BcmShell
