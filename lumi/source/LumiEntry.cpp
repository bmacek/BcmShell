#include "BcmShell/lumi/LumiEntry.h"
#include "BcmShell/lumi/LumiData.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiEntry, CAT_NAME_BCMLUMIENTRY, CAT_TYPE_BCMLUMIENTRY);

LumiEntry::LumiEntry()
{
}

LumiEntry::~LumiEntry()
{
}

void LumiEntry::cat_stream_out(std::ostream& output)
{
	output.write((char*)&_entry_id, sizeof(std::uint32_t));
	CatShell::PluginDeamon::stream_object_out(output, _period);

	std::uint32_t tmp = _events.size();
	output.write((char*)&tmp, sizeof(std::uint32_t));
	for(auto& x: _events)
		CatShell::PluginDeamon::stream_object_out(output, x);
}

void LumiEntry::cat_stream_in(std::istream& input)
{
	input.read((char*)&_entry_id, sizeof(std::uint32_t));
	_period = CatShell::PluginDeamon::stream_object_in(input);

	std::uint32_t tmp;
	CatPointer<LumiEvent> obj;

	_events.clear();
	input.read((char*)&tmp, sizeof(std::uint32_t));
	for(std::uint32_t i=0; i<tmp; ++i)
	{
		obj = CatShell::PluginDeamon::stream_object_in(input);
		_events.push_back(obj);
	}
}

void LumiEntry::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding); output << "BCM lumi entry." << endl;
	output << padding << "Entry ID : " << _entry_id << endl;
	output << padding << "Events   : "  << _events.size() << endl;
	for(auto& x: _events)
	{
		x->cat_print(output, padding + " |  "); output << endl;
	}
	output << padding << "Period   :" << endl;
	_period->cat_print(output, padding + " |  ");
}

void LumiEntry::read_bcm_native(std::istream& input)
{
	input.read((char*)&_entry_id, sizeof(std::uint32_t));

	std::uint16_t tmp;
	CatPointer<LumiEvent> obj;

	_events.clear();
	input.read((char*)&tmp, sizeof(std::uint16_t));
	for(std::uint32_t i=0; i<tmp; ++i)
	{
		obj = LumiData::readLegacyEvent(input);
		_events.push_back(obj);
	}

	_period = LumiData::readLegacyPeriod(input);
}

void LumiEntry::cat_free() 
{
	_period.make_nullptr();
	_events.clear();
}

StreamSize LumiEntry::cat_stream_get_size() const
{
	StreamSize size = sizeof(std::uint32_t);
	size += CatShell::PluginDeamon::stream_object_size(_period);
	for(auto& x: _events)
		size += CatShell::PluginDeamon::stream_object_size(x);

	return size;
}

} // namespace BcmShell
