#include "BcmShell/lumi/LumiTranslate.h"
#include "BcmShell/lumi/LumiEntry.h"
#include "BcmShell/lumi/LumiBcidMap.h"
#include "BcmShell/lumi/LumiPeriod.h"
#include "BcmShell/lumi/LumiPeriodBlank.h"
#include "BcmShell/lumi/LumiEvent.h"
#include "BcmShell/lumi/LumiForcedEvent.h"
#include "BcmShell/lumi/LumiLbChange.h"
#include "BcmShell/lumi/LumiPlbChange.h"
#include "BcmShell/lumi/LumiNothing.h"

#include "CatShell/core/PluginDeamon.h"

#include <math.h>

using namespace std;
using namespace CatShell;
using namespace LumiShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiTranslate, CAT_NAME_BCMLUMITRANSLATE, CAT_TYPE_BCMLUMITRANSLATE);


LumiTranslate::LumiTranslate()
{

}

LumiTranslate::~LumiTranslate()
{

}

CatPointer<DetPeriod> LumiTranslate::translate_period(CatPointer<LumiPeriod>& period)
{
        if(period->cat_type() == CAT_TYPE_BCMLUMIPERIOD)
        {
                CatPointer<DetPeriod> np = _pool_DetPeriod->get_element();
                np->set_start(period->get_start());
                np->set_end(period->get_end());
                return np;
        }
        else if(period->cat_type() == CAT_TYPE_BCMLUMIPERIODBLANK)
        {
                CatPointer<DetPeriodBlank> np = _pool_DetPeriodBlank->get_element();
                np->set_start(period->get_start());
                np->set_end(period->get_end());
                return np;
        }
        else if(period->cat_type() == CAT_TYPE_BCMLUMIBCIDMAP)
        {
            // this is translation to DetLumiMap
            /*
                CatPointer<DetLumiMap> np = _pool_DetLumiMap->get_element();
                CatPointer<LumiBcidMap> data = period.unsafeCast<LumiBcidMap>();

                // transcribe the period
                np->set_start(data->get_start());
                np->set_end(data->get_end());

                // map id & orbits
                np->set_map_id(data->get_mapID());
                np->set_orbits(data->get_orbits());

                double number_of_orbits = data->get_orbits();
                double rate = 0;

                // loop through all the BCs
                for(int i=0; i<ORBIT_LENGTH; ++i)
                {
                        rate = (*data)[i] / number_of_orbits;
                        np->get_value(i) = rate;
                        np->get_error(i) = sqrt(number_of_orbits*rate*(1-rate))/number_of_orbits;
                }

                return np;
            */
            // this is translation to DetLumiCounters
                CatPointer<DetLumiCounters> np = _pool_DetLumiCounters->get_element();
                CatPointer<LumiBcidMap> data = period.unsafeCast<LumiBcidMap>();

                // transcribe the period
                np->set_start(data->get_start());
                np->set_end(data->get_end());

                // map id & orbits
                np->set_map_id(data->get_mapID());
                np->set_orbits(data->get_orbits());

                // loop through all the BCs
                for(int i=0; i<ORBIT_LENGTH; ++i)
                {
                    (*np)[i] = (*data)[i];
                }

                return np;
        }
        else if(period->cat_type() == CAT_TYPE_BCMLUMIREADINGRATE)
        {
                CatPointer<DetPeriodBlank> np = _pool_DetPeriodBlank->get_element();
                np->set_start(period->get_start());
                np->set_end(period->get_end());
                return np;
        }
        else
        {
               CatPointer<DetLumiMap> np; // returns nullptr
               return np; 
        }
}

CatPointer<DetEvent> LumiTranslate::translate_event(CatPointer<LumiEvent>& event)
{
        if(event->cat_type() == CAT_TYPE_BCMLUMIEVENT)
        {
                CatPointer<DetEvent> np = _pool_DetEvent->get_element();
                return np;
        }
        else if(event->cat_type() == CAT_TYPE_BCMLUMIFORCEDEVENT)
        {
                CatPointer<DetForcedEvent> np = _pool_DetForcedEvent->get_element();
                CatPointer<LumiForcedEvent> data = event.unsafeCast<LumiForcedEvent>();
                (*np) = data->text();
                return np;
        }
        else if(event->cat_type() == CAT_TYPE_BCMLUMILBCHANGE)
        {
                CatPointer<DetLbChange> np = _pool_DetLbChange->get_element();
                CatPointer<LumiLbChange> data = event.unsafeCast<LumiLbChange>();
                np->set_run(data->get_run());
                np->set_lumi_block(data->get_lumi_block());
                np->set_time(data->get_time());
                return np;
        }
        else if(event->cat_type() == CAT_TYPE_BCMLUMIPLBCHANGE)
        {
                CatPointer<DetPlbChange> np = _pool_DetPlbChange->get_element();
                CatPointer<LumiPlbChange> data = event.unsafeCast<LumiPlbChange>();
                np->set_run(data->get_run());
                np->set_lumi_block(data->get_lumi_block());
                np->set_time(data->get_time());
                return np;
        }
        else if(event->cat_type() == CAT_TYPE_BCMLUMINOTHING)
        {
                CatPointer<DetNothing> np = _pool_DetNothing->get_element();
                return np;
        }
        else
        {
               CatPointer<DetEvent> np; // returns nullptr
               return np; 
        }
}

void LumiTranslate::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(port == _in_port_map && object.isSomething())
        {
                // it is assumed, that this is lumi entry
                CatPointer<LumiEntry> data = object.unsafeCast<LumiEntry>();

                // create new entry
                CatPointer<DetEntry> entry = _pool_DetEntry->get_element();

                // ID
                entry->set_entry_id(data->get_id());

                // translate the period
                entry->get_period() = translate_period(data->get_period());

                // translate events
                data->loop_events([&](CatShell::CatPointer<LumiEvent>& e){
                        CatPointer<DetEvent> te = translate_event(e);
                        entry->insert_event(te); 
                });

                // publish
                publish_data(_out_port_map, entry, logger);
        }

        Algorithm::process_data(port, object, flush, logger);
}

void LumiTranslate::algorithm_init()
{
        Algorithm::algorithm_init();

        // declare the ports
        _in_port_map = declare_input_port("in_entry", CAT_NAME_BCMLUMIENTRY);
        _out_port_map = declare_output_port("out_entry", CAT_NAME_LUMI_DETLUMIMAP);

        // get the memory pools
        _pool_DetEntry = PluginDeamon::get_pool<DetEntry>();
        _pool_DetPeriod = PluginDeamon::get_pool<DetPeriod>();
        _pool_DetPeriodBlank = PluginDeamon::get_pool<DetPeriodBlank>();
        _pool_DetLumiMap = PluginDeamon::get_pool<DetLumiMap>();
        _pool_DetLumiCounters = PluginDeamon::get_pool<DetLumiCounters>();
        _pool_DetEvent = PluginDeamon::get_pool<DetEvent>();
        _pool_DetForcedEvent = PluginDeamon::get_pool<DetForcedEvent>();
        _pool_DetLbChange = PluginDeamon::get_pool<DetLbChange>();
        _pool_DetPlbChange = PluginDeamon::get_pool<DetPlbChange>();
        _pool_DetNothing = PluginDeamon::get_pool<DetNothing>();
}

void LumiTranslate::algorithm_deinit()
{
        Algorithm::algorithm_deinit();
}

CatPointer<CatObject> LumiTranslate::produce_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
    // get the source data
    CatPointer<LumiEntry> data = request_data(_in_port_map, flush, logger).unsafeCast<LumiEntry>();

    if(data.isSomething())
    {
        // create new entry
        CatPointer<DetEntry> entry = _pool_DetEntry->get_element();

        // ID
        entry->set_entry_id(data->get_id());

        // translate the period
        entry->get_period() = translate_period(data->get_period());

        // translate events
        data->loop_events([&](CatShell::CatPointer<LumiEvent>& e){
            CatPointer<DetEvent> te = translate_event(e);
            entry->insert_event(te); 
        });

        return entry;        
    }
    else
        return CatPointer<DetEntry>(nullptr);
}

} // namespace BcmShell
