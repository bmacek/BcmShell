#include "CatShell/core/MemoryPool.h"
#include "CatShell/core/PluginDeamon.h"
#include "BcmShell/lumi/LumiParser.h"
#include "BcmShell/lumi/LumiEntry.h"

#include <iostream>

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiParser, CAT_NAME_BCMLUMIPARSER, CAT_TYPE_BCMLUMIPARSER);

LumiParser::LumiParser()
{

}

LumiParser::~LumiParser()
{

}

void LumiParser::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
    // will not be processing any data
}

bool LumiParser::open_next_file(CatShell::Logger& logger)
{
        // close the current file
        if(_file)
        {
                delete _file;
                _file = nullptr;
        }

        // construct the file name
        char name[1000];
        sprintf(name, "%s.%u.%s", _name.c_str(), _name_number, _name_extention.c_str());
        _name_number++;

        // open the file
        _file = new ifstream(name, std::ifstream::in | std::ifstream::binary);
        if(_file->is_open())
        {
            LOG(logger, "File opened: " << name);
            // prepare the exception
            _file->exceptions( std::ios::failbit | std::ios::badbit  | std::ios::eofbit );
            return true;
        }
        else
        {
            WARNING(logger, "Could not opened file: " << name);
            delete _file;
            _file = nullptr;
            return false;
        }
}

void LumiParser::algorithm_init()
{
	Process::algorithm_init();

    //
    _name = algo_settings().get_setting<std::string>("file/name", "unnamed");
    _name_extention = algo_settings().get_setting<std::string>("file/extention", ".lumi");
    _name_number = algo_settings().get_setting<std::uint32_t>("file/start", 0);

    // define the output port
    _out_port_entry = declare_output_port("out_entry", CAT_NAME_BCMLUMIENTRY);

    _file = nullptr;

    // set the initial values
    _stat_objects = 0;
    _stat_type_err = 0;
    _stat_memory_err = 0;

    // memeory pool for LumiEntries
    _pool = PluginDeamon::get_pool<LumiEntry>();
}

void LumiParser::algorithm_deinit()
{
	Process::algorithm_deinit();

    // close the current file
    if(_file)
    {
        delete _file;
        _file = nullptr;
    }
}

CatPointer<CatObject> LumiParser::read_next_element(bool& flush, CatShell::Logger& logger)
{
    std::uint8_t    type;
    std::uint8_t    version;
    std::uint32_t   position;

    while(true)
    {
        try
        {
            while(true)
            {

                // read the type
                _file->read((char*)&type, sizeof(std::uint8_t));
                // read the next position
                _file->read((char*)&position, sizeof(std::uint32_t));
                // read version
                _file->read((char*)&version, sizeof(std::uint8_t));
                // check the type
                if(!(type & 0x40))
                {
                    WARNING(logger, "Old lumi format not supported! Skipping ... (file ptr: " << position << ")");
                    //
                    _file->seekg(position);
                }
                // check the type
                else if((type & 0x3F) != LUMI_TYPE_ENTRY)
                {
                    WARNING(logger, "Only lumiEntry parsing supported for now! Skipping ... (file ptr: " << position << ")");
                    //
                    _file->seekg(position);
                    _stat_type_err++;
                }
                else
                {
                    type &= 0x3F;
                    CatPointer<LumiEntry> element = _pool->get_element();

                    // read the lumiEntry
                    element->read_bcm_native(*_file);
                    _stat_objects++;

                    flush = false;
                    return element;
                }
            }
        }
        catch(std::bad_alloc e)
        {
            _stat_memory_err++;

            // not enough space in teh memory pool
            ERROR(logger, "Not enough memory for objects of type: " << (int)type << ".")
        }
        catch(std::ios::failure e)
        {
            // end of file
            LOG(logger, "End of file received.")
            if(!open_next_file(logger))
            {
                // ran out of files
                flush = true;
                return CatPointer<CatObject>(nullptr);
            }
        }
    }
}

void LumiParser::work_main()
{
    std::uint8_t    type;
    std::uint8_t    version;
    std::uint32_t   position;

    MemoryPool<LumiEntry>* pool = PluginDeamon::get_pool<LumiEntry>();
    if(!pool)
        return;

    CatPointer<CatObject> obj;
    bool flush;
    LOG(logger(), "Started reading the stream.")

    if(open_next_file(logger()))
    {

        while(true)
        {
            if(!should_continue(false))
                break;

            obj = LumiParser::read_next_element(flush, logger());
            // check if finished
            if(flush)
                break;
            // publish
            publish_data(_out_port_entry, obj, logger());
        }
    }
    else
    {
        WARNING(logger(), "No files were available.")
    }

    // flush
    flush_data_clients(_out_port_entry, logger());

    LOG(logger(), "Finished reading the stream.")

    START_LOG_LOG(logger())
    LINE_LOG_LOG(logger(), "End statistics:")
    logger().stream() << logger().padding() << "Total objects    : " << (_stat_objects) << endl;
    logger().stream() << logger().padding() << "Type err objects : " << (_stat_type_err) << endl;
    logger().stream() << logger().padding() << "Mem. err objects : " << (_stat_memory_err) << endl;
    END_LOG(logger())
}

CatPointer<CatObject> LumiParser::produce_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
    if(!_file)
    {
        // no file opened yet, so open it now
        if(!open_next_file(logger))
        {
            flush = true;
            return CatPointer<CatObject>(nullptr);
        }
    }

    return read_next_element(flush, logger);
}

void LumiParser::work_init()
{
    INFO(logger(), "LumiParser preparation...")
}

void LumiParser::work_deinit()
{
    INFO(logger(), "LumiParser clean up...")
}

void LumiParser::work_subthread_finished(CatPointer<WorkThread> child)
{
}

} // namespace BcmShell
