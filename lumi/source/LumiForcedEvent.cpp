#include "BcmShell/lumi/LumiForcedEvent.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiForcedEvent, CAT_NAME_BCMLUMIFORCEDEVENT, CAT_TYPE_BCMLUMIFORCEDEVENT);

LumiForcedEvent::LumiForcedEvent()
{
}

LumiForcedEvent::~LumiForcedEvent()
{
}

void LumiForcedEvent::cat_stream_out(std::ostream& output)
{
	LumiEvent::cat_stream_out(output);

	std::uint32_t tmp = _message.length();
	output.write((char*)&tmp, sizeof(std::uint32_t));
	output.write((char*)_message.c_str(), tmp);
}

void LumiForcedEvent::cat_stream_in(std::istream& input)
{
	LumiEvent::cat_stream_in(input);

	std::uint32_t tmp;
	input.read((char*)&tmp, sizeof(std::uint32_t));
	
	char* c = new char[tmp+1];
	input.read(c, tmp);
	c[tmp] = '\0';
	_message = c;
}

void LumiForcedEvent::cat_print(std::ostream& output, const std::string& padding)
{
	LumiEvent::cat_print(output, padding); output << " " << _message;
}

void LumiForcedEvent::read_bcm_native(std::istream& input)
{
	LumiEvent::read_bcm_native(input);

	std::uint32_t tmp;
	input.read((char*)&tmp, sizeof(std::uint32_t));
	
	char* c = new char[tmp+1];
	input.read(c, tmp);
	c[tmp] = '\0';
	_message = c;
}

} // namespace BcmShell
