#include "BcmShell/lumi/LumiPeriod.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiPeriod, CAT_NAME_BCMLUMIPERIOD, CAT_TYPE_BCMLUMIPERIOD);

LumiPeriod::LumiPeriod()
{
}

LumiPeriod::~LumiPeriod()
{
}

void LumiPeriod::cat_stream_out(std::ostream& output)
{
	_start.write_to_stream(output);
	_end.write_to_stream(output);
}

void LumiPeriod::cat_stream_in(std::istream& input)
{
	_start.read_from_stream(input);
	_end.read_from_stream(input);
}

void LumiPeriod::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	CatObject::cat_print(output, padding); output << "BCM lumi period." << endl;
	_start.convert_to_UTC_asci(text);
	output << padding << "Timestamp start (UTC) : " << text << endl;
	_end.convert_to_UTC_asci(text);
	output << padding << "Timestamp end (UTC)   : " << text << endl;
}

void LumiPeriod::read_bcm_native(std::istream& input)
{
	std::int64_t seconds;
	std::uint32_t nanoseconds;

	// start
	input.read((char*)&seconds, 8);
	seconds &= 0xFFFFFFFF;
	input.read((char*)&nanoseconds, 4);
	_start.set_value(seconds, nanoseconds/1000);

	// end
	input.read((char*)&seconds, 8);
	seconds &= 0xFFFFFFFF;
	input.read((char*)&nanoseconds, 4);
	_end.set_value(seconds, nanoseconds/1000);
}

} // namespace BcmShell
