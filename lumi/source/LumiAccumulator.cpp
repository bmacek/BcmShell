#include "BcmShell/lumi/LumiAccumulator.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiAccumulator, CAT_NAME_BCMLUMIACCUMULATOR, CAT_TYPE_BCMLUMIACCUMULATOR);


LumiAccumulator::LumiAccumulator()
{

}

LumiAccumulator::~LumiAccumulator()
{

}

CatPointer<LumiEntry> LumiAccumulator::process_next_entry(CatPointer<LumiEntry>& data)
{
        CatPointer<LumiEntry>   result(nullptr);
        CatPointer<LumiEvent>   event;

        // check if this entry has this type of event
        data->loop_events([&](CatPointer<LumiEvent>& e){
                if(e->cat_type() == _accumulation_type)
                        event = e;
        }); 

        if(event.isSomething())
        {
                // there is an event relevant for this accumulation
                if(_entry.isSomething())
                {
                        // already accumulating
                        _entry->get_period()->set_end(data->get_period()->get_start());
                        _entry->set_id(data->get_id());
                        result = _entry;
                        //publish_data(_out_port_map, _entry, logger);
                }

                // get new entry
                _entry = _pool->get_element();

                // insert event
                _entry->insert_event(event);

                // make an empty accumulation map
                CatPointer<LumiBcidMap> map = _pool_maps->get_element();
                map->reset(0);
                map->set_start(data->get_period()->get_end());
                map->set_end(data->get_period()->get_end());
                
                _entry->get_period() = map;
                _entry->set_id(data->get_id());
        }
        else
        {
                // no such event found
                if(_entry.isSomething())
                {
                        // already accumulating
                        // add to the accumulation

                        if(data->get_period()->cat_type() == CAT_TYPE_BCMLUMIBCIDMAP)
                        {
                                // if map, than accumulate it
                                CatPointer<LumiBcidMap> map_entry = _entry->get_period().unsafeCast<LumiBcidMap>();
                                CatPointer<LumiBcidMap> map_data = data->get_period().unsafeCast<LumiBcidMap>();

                                map_entry->add(map_data);
                        }
                }
                else
                {
                        // not accumulating yet
                        // do nothing
                }
        }

        return result;
}

void LumiAccumulator::process_data(DataPort port, CatPointer<CatObject>& object, bool flush, CatShell::Logger& logger)
{
        if(_accumulation_type != CAT_TYPE_OBJECT && object.isSomething())
        {
                CatPointer<LumiEntry>   data = object.unsafeCast<LumiEntry>();
                if(port == _in_port_map)
                {
                        CatPointer<LumiEntry> result = process_next_entry(data);
                        if(result.isSomething())
                                publish_data(_out_port_map, result, logger);
                }
                else
                        WARNING(logger, "Unknown port!")    
        }

        Algorithm::process_data(port, object, flush, logger);

        if(flush)
        {
                _entry.make_nullptr();
        }
}

void LumiAccumulator::algorithm_init()
{
        Algorithm::algorithm_init();

        _pool = PluginDeamon::get_pool<LumiEntry>();
        _pool_maps = PluginDeamon::get_pool<LumiBcidMap>();

        _in_port_map = declare_input_port("in_entry", CAT_NAME_BCMLUMIENTRY);
        _out_port_map = declare_output_port("out_entry", CAT_NAME_BCMLUMIENTRY);

        std::string type_name = algo_settings().get_setting<std::string>("type", "LumiLbChange");

        // convert name to type number
        MemoryPoolBase* mem_pool = PluginDeamon::get_memory_pool(type_name);
        if(!mem_pool)
        {
                _accumulation_type = CAT_TYPE_OBJECT;
                return;
        }
        _accumulation_type = mem_pool->memory_cat_type();
}

CatPointer<CatObject> LumiAccumulator::produce_data(DataPort port, bool& flush, CatShell::Logger& logger)
{
        if(port == _out_port_map)
        {
                // get the source data
                CatPointer<LumiEntry> data;
                CatPointer<LumiEntry> result(nullptr);

                while(true)
                {
                        data = request_data(_in_port_map, flush, logger).unsafeCast<LumiEntry>();
                        if(flush)
                                return result;
                        result = process_next_entry(data);
                        if(result.isSomething())
                                return result;
                }
        }

        return CatPointer<LumiEntry>(nullptr);
}

} // namespace BcmShell
