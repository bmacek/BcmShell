#include "BcmShell/lumi/LumiData.h"
#include "BcmShell/lumi/LumiForcedEvent.h"
#include "BcmShell/lumi/LumiLbChange.h"
#include "BcmShell/lumi/LumiPlbChange.h"
#include "BcmShell/lumi/LumiNothing.h"
#include "BcmShell/lumi/LumiBcidMap.h"
#include "BcmShell/lumi/LumiPeriodBlank.h"
#include "BcmShell/lumi/LumiReadingRate.h"

#include "CatShell/core/PluginDeamon.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

LumiData::LumiData()
{
}

LumiData::~LumiData()
{
}

CatPointer<LumiEvent> LumiData::getLegacyInstanceOfEvent(std::uint8_t typeOfData)
{
	switch(typeOfData)
	{
	case LUMI_TYPE_EVENT_FORCED:
		return PluginDeamon::get_pool<LumiForcedEvent>()->get_element().unsafeCast<LumiEvent>();
	case LUMI_TYPE_LB_CHANGE:
		return PluginDeamon::get_pool<LumiLbChange>()->get_element().unsafeCast<LumiEvent>();
	case LUMI_TYPE_PSEUDO_LB_CHANGE:
		return PluginDeamon::get_pool<LumiPlbChange>()->get_element().unsafeCast<LumiEvent>();
	case LUMI_TYPE_EVENT_NOTHING:
	case LUMI_TYPE_EVENT_MISSINGPERIOD:
	case LUMI_TYPE_EVENT_ORBITOVERFLOW:
		return PluginDeamon::get_pool<LumiNothing>()->get_element().unsafeCast<LumiEvent>();
	default:
		return CatPointer<LumiEvent>(nullptr);
	}
}

CatPointer<LumiEvent> LumiData::readLegacyEvent(std::istream& input)
{
	std::uint8_t type, version;
	std::uint32_t next;

	input.read((char*)&type, sizeof(std::uint8_t));
	input.read((char*)&next, sizeof(std::uint32_t));
	input.read((char*)&version, sizeof(std::uint8_t));

	type &= 0x3F;

	CatPointer<LumiEvent> obj = LumiData::getLegacyInstanceOfEvent(type);
	obj->read_bcm_native(input);

	return obj;
}

CatPointer<LumiPeriod> LumiData::getLegacyInstanceOfPeriod(std::uint8_t typeOfData)
{
	switch(typeOfData)
	{
	case LUMI_TYPE_MAPBCIDACCU:
		return PluginDeamon::get_pool<LumiBcidMap>()->get_element().unsafeCast<LumiPeriod>();
	case LUMI_TYPE_BLANK:
		return PluginDeamon::get_pool<LumiPeriodBlank>()->get_element().unsafeCast<LumiPeriod>();
	case LUMI_TYPE_READING_RATE:
		return PluginDeamon::get_pool<LumiReadingRate>()->get_element().unsafeCast<LumiPeriod>();
	default:
		return CatPointer<LumiPeriod>(nullptr);
	}
}

CatPointer<LumiPeriod> LumiData::readLegacyPeriod(std::istream& input)
{
	std::uint8_t type, version;
	std::uint32_t next;

	input.read((char*)&type, sizeof(std::uint8_t));
	input.read((char*)&next, sizeof(std::uint32_t));
	input.read((char*)&version, sizeof(std::uint8_t));

	type &= 0x3F;

	CatPointer<LumiPeriod> obj = LumiData::getLegacyInstanceOfPeriod(type);
	obj->read_bcm_native(input);

	return obj;
}

} // namespace BcmShell
