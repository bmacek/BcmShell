#include "BcmShell/lumi/LumiLbChange.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiLbChange, CAT_NAME_BCMLUMILBCHANGE, CAT_TYPE_BCMLUMILBCHANGE);

LumiLbChange::LumiLbChange()
{
}

LumiLbChange::~LumiLbChange()
{
}

void LumiLbChange::cat_stream_out(std::ostream& output)
{
	LumiEvent::cat_stream_out(output);

	output.write((char*)&_run_number, sizeof(std::uint32_t));
	output.write((char*)&_lumi_block, sizeof(std::uint16_t));
	_time.write_to_stream(output);
}

void LumiLbChange::cat_stream_in(std::istream& input)
{
	LumiEvent::cat_stream_in(input);

	input.read((char*)&_run_number, sizeof(std::uint32_t));
	input.read((char*)&_lumi_block, sizeof(std::uint16_t));
	_time.read_from_stream(input);
}

void LumiLbChange::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	LumiEvent::cat_print(output, padding); output << "Lumi LB change." << endl;
	output << padding << "Run number     : " << _run_number << endl;
	output << padding << "Lumi block     : " << _lumi_block << endl;
	_time.convert_to_UTC_asci(text);
	output << padding << "Timestamp (UTC): " << text;
}

void LumiLbChange::read_bcm_native(std::istream& input)
{
	std::int64_t seconds;
	std::uint32_t nanoseconds;

	LumiEvent::read_bcm_native(input);

	input.read((char*)&_run_number, sizeof(std::uint32_t));
	input.read((char*)&_lumi_block, sizeof(std::uint16_t));
	// start

	input.read((char*)&seconds, 8);
	seconds &= 0xFFFFFFFF;
	input.read((char*)&nanoseconds, 4);
	_time.set_value(seconds, nanoseconds/1000);
}

} // namespace BcmShell
