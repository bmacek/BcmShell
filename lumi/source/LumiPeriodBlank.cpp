#include "BcmShell/lumi/LumiPeriodBlank.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiPeriodBlank, CAT_NAME_BCMLUMIPERIODBLANK, CAT_TYPE_BCMLUMIPERIODBLANK);

LumiPeriodBlank::LumiPeriodBlank()
{
}

LumiPeriodBlank::~LumiPeriodBlank()
{
}

void LumiPeriodBlank::cat_stream_out(std::ostream& output)
{
	LumiPeriod::cat_stream_out(output);
}

void LumiPeriodBlank::cat_stream_in(std::istream& input)
{
	LumiPeriod::cat_stream_in(input);
}

void LumiPeriodBlank::cat_print(std::ostream& output, const std::string& padding)
{
	char text[100];

	LumiPeriod::cat_print(output, padding); output << "Empty BCM lumi period." << endl;
}

void LumiPeriodBlank::read_bcm_native(std::istream& input)
{
	LumiPeriod::read_bcm_native(input);
}

} // namespace BcmShell
