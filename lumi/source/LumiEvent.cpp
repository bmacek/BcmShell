#include "BcmShell/lumi/LumiEvent.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiEvent, CAT_NAME_BCMLUMIEVENT, CAT_TYPE_BCMLUMIEVENT);

LumiEvent::LumiEvent()
{
}

LumiEvent::~LumiEvent()
{
}

void LumiEvent::cat_stream_out(std::ostream& output)
{
}

void LumiEvent::cat_stream_in(std::istream& input)
{
}

void LumiEvent::cat_print(std::ostream& output, const std::string& padding)
{
	CatObject::cat_print(output, padding); output << "BCM lumi event.";
}

void LumiEvent::read_bcm_native(std::istream& input)
{
}

} // namespace BcmShell
