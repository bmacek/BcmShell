#include "BcmShell/lumi/LumiNothing.h"

using namespace std;
using namespace CatShell;

namespace BcmShell {

CAT_OBJECT_IMPLEMENT(LumiNothing, CAT_NAME_BCMLUMINOTHING, CAT_TYPE_BCMLUMINOTHING);

LumiNothing::LumiNothing()
{
}

LumiNothing::~LumiNothing()
{
}

void LumiNothing::cat_stream_out(std::ostream& output)
{
	LumiEvent::cat_stream_out(output);
}

void LumiNothing::cat_stream_in(std::istream& input)
{
	LumiEvent::cat_stream_in(input);
}

void LumiNothing::cat_print(std::ostream& output, const std::string& padding)
{
	LumiEvent::cat_print(output, padding); output << " Nothing happened.";
}

void LumiNothing::read_bcm_native(std::istream& input)
{
	LumiEvent::read_bcm_native(input);
}

} // namespace BcmShell
