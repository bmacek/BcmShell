#include "CatShell/core/Plugin.h"
#include "BcmShell/lumi/LumiPeriod.h"
#include "BcmShell/lumi/LumiPeriodBlank.h"
#include "BcmShell/lumi/LumiReadingRate.h"
#include "BcmShell/lumi/LumiBcidMap.h"
#include "BcmShell/lumi/LumiEntry.h"
#include "BcmShell/lumi/LumiEvent.h"
#include "BcmShell/lumi/LumiForcedEvent.h"
#include "BcmShell/lumi/LumiLbChange.h"
#include "BcmShell/lumi/LumiNothing.h"
#include "BcmShell/lumi/LumiParser.h"
#include "BcmShell/lumi/LumiPlbChange.h"
#include "BcmShell/lumi/LumiAccumulator.h"
#include "BcmShell/lumi/LumiTranslate.h"

using namespace std;

namespace BcmShell {

EXPORT_PLUGIN(CatShell::Plugin, BcmLumiPlugin)
START_EXPORT_CLASSES(BcmLumiPlugin)
EXPORT_CLASS(LumiPeriod, CAT_NAME_BCMLUMIPERIOD, BcmLumiPlugin)
EXPORT_CLASS(LumiPeriodBlank, CAT_NAME_BCMLUMIPERIODBLANK, BcmLumiPlugin)
EXPORT_CLASS(LumiReadingRate, CAT_NAME_BCMLUMIREADINGRATE, BcmLumiPlugin)
EXPORT_CLASS(LumiBcidMap, CAT_NAME_BCMLUMIBCIDMAP, BcmLumiPlugin)
EXPORT_CLASS(LumiEntry, CAT_NAME_BCMLUMIENTRY, BcmLumiPlugin)
EXPORT_CLASS(LumiEvent, CAT_NAME_BCMLUMIEVENT, BcmLumiPlugin)
EXPORT_CLASS(LumiForcedEvent, CAT_NAME_BCMLUMIFORCEDEVENT, BcmLumiPlugin)
EXPORT_CLASS(LumiLbChange, CAT_NAME_BCMLUMILBCHANGE, BcmLumiPlugin)
EXPORT_CLASS(LumiNothing, CAT_NAME_BCMLUMINOTHING, BcmLumiPlugin)
EXPORT_CLASS(LumiParser, CAT_NAME_BCMLUMIPARSER, BcmLumiPlugin)
EXPORT_CLASS(LumiPlbChange, CAT_NAME_BCMLUMIPLBCHANGE, BcmLumiPlugin)
EXPORT_CLASS(LumiAccumulator, CAT_NAME_BCMLUMIACCUMULATOR, BcmLumiPlugin)
EXPORT_CLASS(LumiTranslate, CAT_NAME_BCMLUMITRANSLATE, BcmLumiPlugin)
END_EXPORT_CLASSES(BcmLumiPlugin)

} // namespace DbmShell