#ifndef INCLUDE_BCMLUMILBCHANGE_INCLUDED
#define INCLUDE_BCMLUMILBCHANGE_INCLUDED

#include "CatShell/core/CatObject.h"
#include "CatShell/core/TimePoint.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiEvent.h"

namespace BcmShell {

class LumiLbChange : public LumiEvent
///
/// Accumulation period for BCM.
///
{
        CAT_OBJECT_DECLARE(LumiLbChange, CAT_NAME_BCMLUMILBCHANGE, CAT_TYPE_BCMLUMILBCHANGE);
public:

        LumiLbChange();
                /// Constructor: default.

        virtual ~LumiLbChange();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        virtual void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

        std::uint32_t get_run() const;
                /// Returns the run number.

        std::uint16_t get_lumi_block() const;
                /// Returns the number of the lumi block.

        const CatShell::TimePoint& get_time() const;
                /// Returns the time of the lumi block change.

protected:

private:

        std::uint32_t           _run_number;
        std::uint16_t           _lumi_block;
        CatShell::TimePoint     _time;
};

inline void LumiLbChange::cat_free() {}
inline StreamSize LumiLbChange::cat_stream_get_size() const { return LumiEvent::cat_stream_get_size() + _time.get_stream_length() + sizeof(std::uint32_t) + sizeof(std::uint16_t); }
inline std::uint32_t LumiLbChange::get_run() const { return _run_number; }
inline std::uint16_t LumiLbChange::get_lumi_block() const { return _lumi_block; }
inline const CatShell::TimePoint& LumiLbChange::get_time() const { return _time; }

} // namespace BcmShell

#endif
