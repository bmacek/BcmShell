#ifndef INCLUDE_BCMLUMIREADINGRATE_INCLUDED
#define INCLUDE_BCMLUMIREADINGRATE_INCLUDED

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatService.h"


#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiPeriod.h"

namespace BcmShell {

class LumiReadingRate : public LumiPeriod
///
/// Accumulation period where no data could be found.
///
{
        CAT_OBJECT_DECLARE(LumiReadingRate, CAT_NAME_BCMLUMIREADINGRATE, CAT_TYPE_BCMLUMIREADINGRATE);
public:

        LumiReadingRate();
                /// Constructor: default.

        virtual ~LumiReadingRate();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

protected:

private:

        std::uint32_t   _counts;
        std::uint32_t   _orbits;

};

inline void LumiReadingRate::cat_free() {}
inline StreamSize LumiReadingRate::cat_stream_get_size() const { return LumiPeriod::cat_stream_get_size() + CatShell::CatService::lengtOf7bitEncoded(_counts) + CatShell::CatService::lengtOf7bitEncoded(_orbits); }

} // namespace BcmShell

#endif
