#ifndef INCLUDE_BCMLUMIBCIDMAP_INCLUDED
#define INCLUDE_BCMLUMIBCIDMAP_INCLUDED

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiPeriod.h"

namespace BcmShell {

class LumiBcidMap : public LumiPeriod
///
/// Accumulation period where no data could be found.
///
{
        CAT_OBJECT_DECLARE(LumiBcidMap, CAT_NAME_BCMLUMIBCIDMAP, CAT_TYPE_BCMLUMIBCIDMAP);
public:

        LumiBcidMap();
                /// Constructor: default.

        virtual ~LumiBcidMap();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

        void reset(std::uint8_t map_id);
                /// Resets all the counters.

        void add(CatShell::CatPointer<LumiBcidMap>& map);
                /// Adds counters from second map. It also takes map-ID of the 'map'.

        std::uint8_t get_mapID() const;
                /// Returns the ID of the map.

        std::uint32_t get_orbits() const;
                /// Returns the number of obrits over which this data has been collected.

        std::uint32_t& operator[](int i);
                /// Returnst the i-th counter.

protected:

private:

        std::uint8_t    _map_id;
        std::uint32_t   _orbits;
        std::uint32_t   _counters[ORBIT_LENGTH];
};

inline void LumiBcidMap::cat_free() {}
inline std::uint8_t LumiBcidMap::get_mapID() const { return _map_id; }
inline std::uint32_t LumiBcidMap::get_orbits() const { return _orbits; }
inline std::uint32_t& LumiBcidMap::operator[](int i) { return _counters[i]; }


} // namespace BcmShell

#endif
