#ifndef INCLUDE_BCMLUMIENTRY_INCLUDED
#define INCLUDE_BCMLUMIENTRY_INCLUDED

#include "CatShell/core/CatObject.h"
#include "CatShell/core/CatPointer.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiEvent.h"
#include "BcmShell/lumi/LumiPeriod.h"

namespace BcmShell {

class LumiEntry : public CatShell::CatObject
///
/// Luminosity entity, with period and events.
///
{
        CAT_OBJECT_DECLARE(LumiEntry, CAT_NAME_BCMLUMIENTRY, CAT_TYPE_BCMLUMIENTRY);
public:

        LumiEntry();
                /// Constructor: default.

        virtual ~LumiEntry();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        virtual void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

        void loop_events(std::function<void (CatShell::CatPointer<LumiEvent>&)> f);
                /// Calls the function for all events.

        void insert_event(CatShell::CatPointer<LumiEvent>& event);
                /// Insert event in the list of events.

        CatShell::CatPointer<LumiPeriod>& get_period();
                /// Returns the pointer to the period.

        std::uint32_t get_id() const;
                /// Returns unique id.

        void set_id(std::uint32_t id);
                /// Sets unique id.

protected:

private:

        std::uint32_t                                   _entry_id;
        CatShell::CatPointer<LumiPeriod>                _period;
        std::vector<CatShell::CatPointer<LumiEvent>>    _events;

};

inline void LumiEntry::loop_events(std::function<void (CatShell::CatPointer<LumiEvent>&)> f) { for(auto& x: _events) f(x); }
inline void LumiEntry::insert_event(CatShell::CatPointer<LumiEvent>& event) { _events.push_back(event); }
inline CatShell::CatPointer<LumiPeriod>& LumiEntry::get_period() { return _period; }
inline std::uint32_t LumiEntry::get_id() const { return _entry_id; }
inline void LumiEntry::set_id(std::uint32_t id) { _entry_id = id; }


} // namespace BcmShell

#endif
