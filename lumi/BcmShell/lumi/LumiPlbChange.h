#ifndef INCLUDE_BCMLUMIPLBCHANGE_INCLUDED
#define INCLUDE_BCMLUMIPLBCHANGE_INCLUDED

#include "CatShell/core/CatObject.h"
#include "CatShell/core/TimePoint.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiEvent.h"

namespace BcmShell {

class LumiPlbChange : public LumiEvent
///
/// Accumulation period for BCM.
///
{
        CAT_OBJECT_DECLARE(LumiPlbChange, CAT_NAME_BCMLUMIPLBCHANGE, CAT_TYPE_BCMLUMIPLBCHANGE);
public:

        LumiPlbChange();
                /// Constructor: default.

        virtual ~LumiPlbChange();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        virtual void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

        std::uint32_t get_run() const;
                /// Returns the run number.

        std::uint16_t get_lumi_block() const;
                /// Returns the number of the lumi block.

        const CatShell::TimePoint& get_time() const;
                /// Returns the time of the lumi block change.

protected:

private:

        std::uint32_t           _run_number;
        std::uint16_t           _lumi_block;
        CatShell::TimePoint     _time;
};

inline void LumiPlbChange::cat_free() {}
inline StreamSize LumiPlbChange::cat_stream_get_size() const { return LumiEvent::cat_stream_get_size() + _time.get_stream_length() + sizeof(std::uint32_t) + sizeof(std::uint16_t); }
inline std::uint32_t LumiPlbChange::get_run() const { return _run_number; }
inline std::uint16_t LumiPlbChange::get_lumi_block() const { return _lumi_block; }
inline const CatShell::TimePoint& LumiPlbChange::get_time() const { return _time; }

} // namespace BcmShell

#endif
