#ifndef INCLUDE_BCMLUMIPERIOD_INCLUDED
#define INCLUDE_BCMLUMIPERIOD_INCLUDED

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/CatObject.h"

#include "BcmShell/lumi/defines.h"

namespace BcmShell {

class LumiPeriod : public CatShell::CatObject
///
/// Accumulation period for BCM.
///
{
        CAT_OBJECT_DECLARE(LumiPeriod, CAT_NAME_BCMLUMIPERIOD, CAT_TYPE_BCMLUMIPERIOD);
public:

        LumiPeriod();
                /// Constructor: default.

        virtual ~LumiPeriod();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        virtual void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

        void set_start(CatShell::TimePoint time);
                /// Sets the start of the period.

        void set_end(CatShell::TimePoint time);
                /// Sets the end of the period.

        const CatShell::TimePoint& get_start() const;
                /// Returns the start time of the period.

        const CatShell::TimePoint& get_end() const;
                /// Returns the end time of the period.

protected:

private:

        CatShell::TimePoint     _start;
        CatShell::TimePoint     _end;
};

inline void LumiPeriod::cat_free() {}
inline StreamSize LumiPeriod::cat_stream_get_size() const { return _start.get_stream_length() + _end.get_stream_length(); }
inline void LumiPeriod::set_start(CatShell::TimePoint time) { _start = time; }
inline void LumiPeriod::set_end(CatShell::TimePoint time) { _end = time; }
inline const CatShell::TimePoint& LumiPeriod::get_start() const { return _start; }
inline const CatShell::TimePoint& LumiPeriod::get_end() const { return _end; }

} // namespace BcmShell

#endif
