#ifndef INCLUDE_BCM_LUMIACCUMULATOR
#define INCLUDE_BCM_LUMIACCUMULATOR

#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiEntry.h"
#include "BcmShell/lumi/LumiBcidMap.h"

namespace BcmShell {

class LumiAccumulator : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(LumiAccumulator, CAT_NAME_BCMLUMIACCUMULATOR, CAT_TYPE_BCMLUMIACCUMULATOR);
public:

        LumiAccumulator();
                /// Constructor: default.

        virtual ~LumiAccumulator();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatShell::CatPointer<CatShell::CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request. Secont parameter that is returned if a flush flag.
                /// Object is taken form the first one that producess a result.
//----------------------------------------------------------------------

protected:
private:

        CatShell::CatPointer<LumiEntry> process_next_entry(CatShell::CatPointer<LumiEntry>& data);
                /// Process the next entry.

private:

        CatType                          _accumulation_type;

        CatShell::MemoryPool<LumiEntry>*           _pool;
        CatShell::MemoryPool<LumiBcidMap>*         _pool_maps;

        CatShell::CatPointer<LumiEntry>         _entry;

        CatShell::Algorithm::DataPort    _in_port_map;
        CatShell::Algorithm::DataPort    _out_port_map;
};

inline StreamSize LumiAccumulator::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void LumiAccumulator::cat_stream_out(std::ostream& output) { return CatShell::Algorithm::cat_stream_out(output); }
inline void LumiAccumulator::cat_stream_in(std::istream& input) { return CatShell::Algorithm::cat_stream_in(input); }
inline void LumiAccumulator::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void LumiAccumulator::cat_free() { _entry.make_nullptr(); }
inline void LumiAccumulator::algorithm_deinit() { Algorithm::algorithm_deinit(); }

} // namespace BcmShell

#endif

