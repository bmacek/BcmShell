#ifndef INCLUDE_BCMLUMIPERIODBLANK_INCLUDED
#define INCLUDE_BCMLUMIPERIODBLANK_INCLUDED

#include "CatShell/core/TimePoint.h"
#include "CatShell/core/CatObject.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiPeriod.h"

namespace BcmShell {

class LumiPeriodBlank : public LumiPeriod
///
/// Accumulation period where no data could be found.
///
{
        CAT_OBJECT_DECLARE(LumiPeriodBlank, CAT_NAME_BCMLUMIPERIODBLANK, CAT_TYPE_BCMLUMIPERIODBLANK);
public:

        LumiPeriodBlank();
                /// Constructor: default.

        virtual ~LumiPeriodBlank();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

protected:

private:
};

inline void LumiPeriodBlank::cat_free() {}
inline StreamSize LumiPeriodBlank::cat_stream_get_size() const { return LumiPeriod::cat_stream_get_size(); }

} // namespace BcmShell

#endif
