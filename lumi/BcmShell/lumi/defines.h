#ifndef INCLUDE_BCMLUMIDEFINES
#define INCLUDE_BCMLUMIDEFINES

#include "LumiShell/common/defines.h"

//
// constants
//

#define ORBIT_LENGTH	(3564)


//
// types
//

#define CAT_TYPE_BCMLUMIPERIOD		(0x0000100c)
#define CAT_NAME_BCMLUMIPERIOD		"LumiPeriod"
#define CAT_TYPE_BCMLUMIPERIODBLANK	(0x0000100d)
#define CAT_NAME_BCMLUMIPERIODBLANK	"LumiPeriodBlank"
#define CAT_TYPE_BCMLUMIBCIDMAP		(0x0000100e)
#define CAT_NAME_BCMLUMIBCIDMAP		"LumiBcidMap"
#define CAT_TYPE_BCMLUMIREADINGRATE	(0x0000100f)
#define CAT_NAME_BCMLUMIREADINGRATE	"LumiReadingRate"
#define CAT_TYPE_BCMLUMIEVENT		(0x00001010)
#define CAT_NAME_BCMLUMIEVENT		"LumiEvent"
#define CAT_TYPE_BCMLUMIFORCEDEVENT	(0x00001011)
#define CAT_NAME_BCMLUMIFORCEDEVENT	"LumiForcedEvent"
#define CAT_TYPE_BCMLUMILBCHANGE	(0x00001012)
#define CAT_NAME_BCMLUMILBCHANGE	"LumiLbChange"
#define CAT_TYPE_BCMLUMIPLBCHANGE	(0x00001013)
#define CAT_NAME_BCMLUMIPLBCHANGE	"LumiPlbChange"
#define CAT_TYPE_BCMLUMINOTHING		(0x00001014)
#define CAT_NAME_BCMLUMINOTHING		"LumiNothing"
#define CAT_TYPE_BCMLUMIENTRY		(0x00001015)
#define CAT_NAME_BCMLUMIENTRY		"LumiEntry"
#define CAT_TYPE_BCMLUMIPARSER		(0x00001016)
#define CAT_NAME_BCMLUMIPARSER		"LumiParser"
#define CAT_TYPE_BCMLUMIACCUMULATOR	(0x00001017)
#define CAT_NAME_BCMLUMIACCUMULATOR	"LumiAccumulator"
#define CAT_TYPE_BCMLUMITRANSLATE	(0x0000101a)
#define CAT_NAME_BCMLUMITRANSLATE	"LumiTranslate"


//
// types in legacy stream
//

#define LUMI_TYPE_ABSTRACT                      0x01
#define LUMI_TYPE_MAPBCIDACCU                   0x02
#define LUMI_TYPE_EVENT                         0x03
#define LUMI_TYPE_EVENT_NOTHING                 0x04
#define LUMI_TYPE_EVENT_MISSINGPERIOD           0x05
#define LUMI_TYPE_EVENT_ORBITOVERFLOW           0x06
#define LUMI_TYPE_PERIOD                        0x07
#define LUMI_TYPE_BLANK                         0x08
#define LUMI_TYPE_ENTRY                         0x09
#define LUMI_TYPE_LB_CHANGE                     0x0a
#define LUMI_TYPE_EVENT_FORCED                  0x0b
#define LUMI_TYPE_READING_RATE                  0x0c
#define LUMI_TYPE_LUMI_ACCUMULATION             0x0d
#define LUMI_TYPE_PSEUDO_LB_CHANGE              0x0e

#endif
