#ifndef INCLUDE_BCMLUMIEVENT_INCLUDED
#define INCLUDE_BCMLUMIEVENT_INCLUDED

#include "CatShell/core/CatObject.h"

#include "BcmShell/lumi/defines.h"

namespace BcmShell {

class LumiEvent : public CatShell::CatObject
///
/// Event notable in lumi-stream.
///
{
        CAT_OBJECT_DECLARE(LumiEvent, CAT_NAME_BCMLUMIEVENT, CAT_TYPE_BCMLUMIEVENT);
public:

        LumiEvent();
                /// Constructor: default.

        virtual ~LumiEvent();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        virtual void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

protected:

private:

};

inline void LumiEvent::cat_free() {}
inline StreamSize LumiEvent::cat_stream_get_size() const { return 0; }

} // namespace BcmShell

#endif
