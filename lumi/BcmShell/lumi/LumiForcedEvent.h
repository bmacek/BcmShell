#ifndef INCLUDE_BCMLUMIFORCEDEVENT_INCLUDED
#define INCLUDE_BCMLUMIFORCEDEVENT_INCLUDED

#include "CatShell/core/CatObject.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiEvent.h"

namespace BcmShell {

class LumiForcedEvent : public LumiEvent
///
/// Accumulation period for BCM.
///
{
        CAT_OBJECT_DECLARE(LumiForcedEvent, CAT_NAME_BCMLUMIFORCEDEVENT, CAT_TYPE_BCMLUMIFORCEDEVENT);
public:

        LumiForcedEvent();
                /// Constructor: default.

        virtual ~LumiForcedEvent();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        virtual void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

        const std::string& text() const;
                /// Returns the message of the event.

protected:

private:

        std::string     _message;
};

inline void LumiForcedEvent::cat_free() {}
inline StreamSize LumiForcedEvent::cat_stream_get_size() const { return LumiEvent::cat_stream_get_size() + sizeof(std::uint32_t) + _message.length(); }
inline const std::string& LumiForcedEvent::text() const { return _message; }

} // namespace BcmShell

#endif
