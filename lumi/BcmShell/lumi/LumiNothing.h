#ifndef INCLUDE_BCMLUMINOTHING_INCLUDED
#define INCLUDE_BCMLUMINOTHING_INCLUDED

#include "CatShell/core/CatObject.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiEvent.h"

namespace BcmShell {

class LumiNothing : public LumiEvent
///
/// Nothing happened.
///
{
        CAT_OBJECT_DECLARE(LumiNothing, CAT_NAME_BCMLUMINOTHING, CAT_TYPE_BCMLUMINOTHING);
public:

        LumiNothing();
                /// Constructor: default.

        virtual ~LumiNothing();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------

        virtual void read_bcm_native(std::istream& input);
                /// BACK COMPATIBILITY: for streaming from original BCM format

protected:

private:

};

inline void LumiNothing::cat_free() {}
inline StreamSize LumiNothing::cat_stream_get_size() const { return LumiEvent::cat_stream_get_size(); }

} // namespace BcmShell

#endif
