#ifndef INCLUDE_BCMLUMIDATA_INCLUDED
#define INCLUDE_BCMLUMIDATA_INCLUDED

#include "CatShell/core/CatObject.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiPeriod.h"
#include "BcmShell/lumi/LumiEvent.h"

namespace BcmShell {

class LumiData 
///
/// Service class
///
{
public:

        static CatShell::CatPointer<LumiEvent> getLegacyInstanceOfEvent(std::uint8_t typeOfData);
                /// Returns instance of appropriate LumiPeriod.

        static CatShell::CatPointer<LumiPeriod> getLegacyInstanceOfPeriod(std::uint8_t typeOfData);
                /// Returns instance of appropriate LumiEvent.

        static CatShell::CatPointer<LumiEvent> readLegacyEvent(std::istream& input);
                /// Returns object that was read from the stream.

        static CatShell::CatPointer<LumiPeriod> readLegacyPeriod(std::istream& input);
                /// Returns object that was read from the stream.

protected:
private:

        LumiData();
                /// Constructor: default.

        virtual ~LumiData();
                /// Destructor.
};

} // namespace BcmShell

#endif
