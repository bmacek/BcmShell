#ifndef INCLUDE_LUMIPARSER_INCLUDED
#define INCLUDE_LUMIPARSER_INCLUDED

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiEntry.h"

#include "CatShell/core/Process.h"

namespace BcmShell {

class LumiParser : public CatShell::Process
{
        CAT_OBJECT_DECLARE(LumiParser, CAT_NAME_BCMLUMIPARSER, CAT_TYPE_BCMLUMIPARSER);
public:

        LumiParser();
                /// Constructor: default.

        virtual ~LumiParser();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatShell::CatPointer<CatShell::CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request.
                /// Object is take form the first one that producess a result.

//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void work_main();
                /// Function doing all the work. User should overload this function 
                /// and do all the processing within it.

        virtual void work_init();
                /// Called before the thread starts executing.

        virtual void work_deinit();
                /// Called after the thread stops executing.

        virtual void work_subthread_finished(CatShell::CatPointer<CatShell::WorkThread> child);
                /// Called when a subprocess finished.
//----------------------------------------------------------------------
protected:

private:

        bool open_next_file(CatShell::Logger& logger);
                /// Opens the next file.

        CatShell::CatPointer<CatShell::CatObject> read_next_element(bool& flush, CatShell::Logger& logger);
                /// Reads the next element form the 
private:

        std::string     _name;
        std::string     _name_extention;
        std::uint32_t   _name_number;

        std::ifstream*  _file;

        std::uint32_t   _stat_objects;
        std::uint32_t   _stat_type_err;
        std::uint32_t   _stat_memory_err;

        CatShell::MemoryPool<LumiEntry>*  _pool;

        CatShell::Algorithm::DataPort    _out_port_entry;
};

inline StreamSize LumiParser::cat_stream_get_size() const
{
        return Process::cat_stream_get_size();
}

inline void LumiParser::cat_stream_out(std::ostream& output)
{
        Process::cat_stream_out(output);
}

inline void LumiParser::cat_stream_in(std::istream& input)
{
        Process::cat_stream_in(input);
}

inline void LumiParser::cat_print(std::ostream& output, const std::string& padding)
{
        Process::cat_print(output, padding);
}


} // namespace BcmShell

#endif
