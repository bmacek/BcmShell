#ifndef INCLUDE_BCM_LUMITRANSLATE
#define INCLUDE_BCM_LUMITRANSLATE

#include "CatShell/core/Algorithm.h"
#include "CatShell/core/MemoryPool.h"

#include "BcmShell/lumi/defines.h"
#include "BcmShell/lumi/LumiPeriod.h"
#include "BcmShell/lumi/LumiEvent.h"

#include "LumiShell/common/DetPeriod.h"
#include "LumiShell/common/DetPeriodBlank.h"
#include "LumiShell/common/DetLumiMap.h"
#include "LumiShell/common/DetLumiCounters.h"
#include "LumiShell/common/DetEntry.h"
#include "LumiShell/common/DetEvent.h"
#include "LumiShell/common/DetForcedEvent.h"
#include "LumiShell/common/DetLbChange.h"
#include "LumiShell/common/DetPlbChange.h"
#include "LumiShell/common/DetNothing.h"

namespace BcmShell {

class LumiTranslate : public CatShell::Algorithm
{
        CAT_OBJECT_DECLARE(LumiTranslate, CAT_NAME_BCMLUMITRANSLATE, CAT_TYPE_BCMLUMITRANSLATE);
public:

        LumiTranslate();
                /// Constructor: default.

        virtual ~LumiTranslate();
                /// Destructor.

//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual StreamSize cat_stream_get_size() const;
                /// Should return the size of object when streamed.

        virtual void cat_stream_out(std::ostream& output);
                /// Stream the object to the stream.

        virtual void cat_stream_in(std::istream& input);
                /// Stream the object from the stream.

        virtual void cat_print(std::ostream& output, const std::string& padding);
                /// Streams in human readable format.

        virtual void cat_free();
                /// Free all internal references.
//----------------------------------------------------------------------
//------------THIS FUNCTIONS SHOULD BE OVERWRITTEN----------------------
        virtual void process_data(DataPort port, CatShell::CatPointer<CatShell::CatObject>& object, bool flush, CatShell::Logger& logger);
                /// Process data.

        virtual void algorithm_init();
                /// Defines the process interface and initializes it.

        virtual void algorithm_deinit();
                /// Cleares all the process interfaces.

        virtual CatShell::CatPointer<CatShell::CatObject> produce_data(DataPort port, bool& flush, CatShell::Logger& logger);
                /// Produces data on request.
                /// Object is take form the first one that producess a result.
//----------------------------------------------------------------------

protected:
private:

        CatShell::CatPointer<LumiShell::DetPeriod> translate_period(CatShell::CatPointer<LumiPeriod>& period);
                /// Translates the period from BCM to general.

        CatShell::CatPointer<LumiShell::DetEvent> translate_event(CatShell::CatPointer<LumiEvent>& event);
                /// Translates the event from BCM to general.

private:

        CatShell::MemoryPool<LumiShell::DetPeriod>*     _pool_DetPeriod;
        CatShell::MemoryPool<LumiShell::DetPeriodBlank>* _pool_DetPeriodBlank;
        CatShell::MemoryPool<LumiShell::DetLumiMap>*    _pool_DetLumiMap;
        CatShell::MemoryPool<LumiShell::DetLumiCounters>*    _pool_DetLumiCounters;
        CatShell::MemoryPool<LumiShell::DetEntry>*      _pool_DetEntry;
        CatShell::MemoryPool<LumiShell::DetEvent>*      _pool_DetEvent;
        CatShell::MemoryPool<LumiShell::DetForcedEvent>* _pool_DetForcedEvent;
        CatShell::MemoryPool<LumiShell::DetLbChange>*   _pool_DetLbChange;
        CatShell::MemoryPool<LumiShell::DetPlbChange>*  _pool_DetPlbChange;
        CatShell::MemoryPool<LumiShell::DetNothing>*    _pool_DetNothing;

        CatShell::Algorithm::DataPort                   _in_port_map;
        CatShell::Algorithm::DataPort                   _out_port_map;
};

inline StreamSize LumiTranslate::cat_stream_get_size() const { return CatShell::Algorithm::cat_stream_get_size(); }
inline void LumiTranslate::cat_stream_out(std::ostream& output) { return CatShell::Algorithm::cat_stream_out(output); }
inline void LumiTranslate::cat_stream_in(std::istream& input) { return CatShell::Algorithm::cat_stream_in(input); }
inline void LumiTranslate::cat_print(std::ostream& output, const std::string& padding) { CatShell::Algorithm::cat_print(output, padding); }
inline void LumiTranslate::cat_free() { }

} // namespace BcmShell

#endif

